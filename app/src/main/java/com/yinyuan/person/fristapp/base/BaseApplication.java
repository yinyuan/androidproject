package com.yinyuan.person.fristapp.base;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by Administrator on 2017/9/20.
 */

public class BaseApplication extends Application {


    public static BaseApplication application=null;

    @Override
    public void onCreate() {
        super.onCreate();
        application=this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //解决65536问题
        MultiDex.install(this);
    }

}
