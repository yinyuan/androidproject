package com.yinyuan.person.fristapp.test;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.jwenfeng.library.pulltorefresh.BaseRefreshListener;
import com.jwenfeng.library.pulltorefresh.PullToRefreshLayout;
import com.yinyuan.person.fristapp.R;
import com.yinyuan.person.fristapp.base.OnRecyclerViewItemClickListener;
import com.yinyuan.person.fristapp.base.RecyclerViewHelper;
import com.yinyuan.person.fristapp.test.adapter.RecyclerViewAdapter;
import com.yinyuan.person.fristapp.test.adapter.SringAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2017/9/20.
 */

public class ReciviewAcitivty extends AppCompatActivity {



    private PullToRefreshLayout pullToRefreshLayout;
    private RecyclerView recyclerView;
    private SringAdapter adapter;
    private List<String> list;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reciviewacitivty);
        initView();
    }

    private void initView(){
        pullToRefreshLayout = (PullToRefreshLayout) findViewById(R.id.refresh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_list);
        list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("PullToRefreshLayout"+i);
        }
        adapter = new SringAdapter(this,list);
        RecyclerViewHelper.initRecyclerViewV(this,recyclerView,true,adapter);
        adapter.setmItemClickListener(new OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                adapteronItemClick(position);
            }
        });
        pullToRefreshLayout.autoRefresh();
        pullToRefreshLayout.setRefreshListener(new BaseRefreshListener() {
            @Override
            public void refresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pullToRefreshLayout.finishRefresh();
                    }
                },2000);
            }

            @Override
            public void loadMore() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run(){
                        for (int i = 0; i < 10; i++) {
                            list.add("PullToRefreshLayout"+(list.size()+i));
                        }
                        pullToRefreshLayout.finishLoadMore();
                    }
                },2000);
            }
        });
    }


    public void adapteronItemClick(int postion){
        Toast.makeText(this,"postion: "+postion,Toast.LENGTH_SHORT).show();
    }

}
