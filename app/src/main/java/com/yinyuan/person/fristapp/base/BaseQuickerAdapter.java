package com.yinyuan.person.fristapp.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yinyuan.person.fristapp.R;
import com.yinyuan.person.fristapp.test.adapter.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/9/20.
 */

public abstract class BaseQuickerAdapter<T>  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private OnRecyclerViewItemClickListener mItemClickListener;
    private OnRecyclerViewItemLongClickListener mItemLongClickListener;
    protected Context mContext;
    protected int mLayoutResId;
    protected LayoutInflater mLayoutInflater;
    public List<T> mData;

    public BaseQuickerAdapter(Context context) {
        this(context, null);
    }

    public BaseQuickerAdapter(Context context, List<T> data){
        mLayoutResId = attachLayoutRes();
        if (mLayoutResId == 0) {
            throw new IllegalAccessError("Layout resource ID must be valid!");
        }
        if(data == null) {
            mData = new ArrayList<>();
        } else {
            this.mData = data;
        }
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }
    //
    protected abstract int attachLayoutRes();

    protected abstract void convert(BaseViewHolder holder, T item);

    @Override
    public int getItemCount() {
        int count = mData.size();
        return count;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return getDefItemViewType(position);
    }

    protected int getDefItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        int type = holder.getItemViewType();
        _setFullSpan(holder);

    }

    protected void _setFullSpan(RecyclerView.ViewHolder holder) {
        if (holder.itemView.getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
            StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            params.setFullSpan(true);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager gridManager = ((GridLayoutManager) manager);
            gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int type = getItemViewType(position);
                    return type;
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        BaseViewHolder baseViewHolder;
        baseViewHolder = onCreateDefViewHolder(parent, viewType);
        // 设置用于单项刷新的tag标识
        //baseViewHolder.itemView.setTag(R.id.view_holder_tag, baseViewHolder);
        _initItemClickListener(baseViewHolder);
        return baseViewHolder;
    }

    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        return createBaseViewHolder(parent, mLayoutResId);
    }

    protected BaseViewHolder createBaseViewHolder(ViewGroup parent, int layoutResId) {
        View view = mLayoutInflater.inflate(layoutResId, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        convert((BaseViewHolder) holder, mData.get(holder.getLayoutPosition()));
        //_setDragFixBackground(holder, position);

    }
    /*private int mDragFixCount = 0;  // 固定数量，从0开始算
    private Drawable mDragFixDrawable;

    private void _setDragFixBackground(RecyclerView.ViewHolder holder, int position) {
        if (position < mDragFixCount) {
            holder.itemView.setBackgroundDrawable(mDragFixDrawable);
        }
    }*/

    private void _initItemClickListener(final BaseViewHolder baseViewHolder) {

        if (mItemClickListener != null) {
            baseViewHolder.itemView.setBackgroundResource(R.drawable.recycler_bg);
            baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onItemClick(v, baseViewHolder.getLayoutPosition());
                }
            });
        }
        if (mItemLongClickListener != null) {
            baseViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return mItemLongClickListener.onItemLongClick(v, baseViewHolder.getLayoutPosition());
                }
            });
        }
    }

    public T getItem(int position) {
        return mData.get(position);
    }

    public List<T> getData() {
        return mData;
    }


    /**
     * 更新数据，替换原有数据
     *
     * @param items
     */
    public void updateItems(List<T> items) {
        mData = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    private void _addItem(int position, T item) {
        if (mData == null || mData.size() == 0) {
            mData = new ArrayList<>();
            mData.add(item);
        } else {
            mData.add(position, item);
        }
    }

    private void _addItemList(int position, List<T> items) {
        if (mData == null || mData.size() == 0) {
            mData = new ArrayList<>();
        }
        mData.addAll(position, items);
    }

    public void addItem(T item) {
        _addItem(0, item);
        notifyItemInserted(0);
    }

    public void addItem(T item, int position) {
        position = Math.min(position, mData.size());
        _addItem(position, item);
        notifyItemInserted(_calcPosition(position));
    }
    public void addLastItem(T item) {
        _addItem(mData.size(), item);
        notifyItemInserted(_calcPosition(mData.size()));
    }


    public void addItems(List<T> items) {
        _addItemList(mData.size(), items);
        int position = _calcPosition(mData.size());
        for (T item : items) {
            notifyItemInserted(position++);
        }
    }
    public void addItems(List<T> items, int position) {
        position = Math.min(position, mData.size());
        _addItemList(position, items);
        int pos = _calcPosition(position);
        for (T item : items) {
            notifyItemInserted(pos++);
        }
    }

    public void removeItem(int position) {
        if (position > mData.size() - 1) {
            return;
        }
        int pos = _calcPosition(position);
        mData.remove(position);
        notifyItemRemoved(pos);
    }

    public void removeItem(T item) {
        int pos = 0;
        for (T info : mData) {
            if (item.hashCode() == info.hashCode()) {
                removeItem(pos);
                break;
            }
            pos++;
        }
    }
    /**
     * 计算位置，算上头部
     * @param position
     * @return
     */
    private int _calcPosition(int position) {
        return position;
    }

    public OnRecyclerViewItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnRecyclerViewItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public OnRecyclerViewItemLongClickListener getmItemLongClickListener() {
        return mItemLongClickListener;
    }

    public void setmItemLongClickListener(OnRecyclerViewItemLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }
}
