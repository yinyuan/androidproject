package com.yinyuan.person.fristapp.test.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.yinyuan.person.fristapp.R;
import com.yinyuan.person.fristapp.base.BaseQuickerAdapter;
import com.yinyuan.person.fristapp.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/9/20.
 */

public class SringAdapter extends BaseQuickerAdapter<String>{


    public SringAdapter(Context context) {
        super(context);
    }

    public SringAdapter(Context context, List<String> data) {
        super(context, data);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_list_item;
    }

    @Override
    protected void convert(BaseViewHolder holder, String item) {
              holder.setText(R.id.tv_name,item);
    }


}
